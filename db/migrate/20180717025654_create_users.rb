class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users, force: :cascade do |t|
      t.string :name
      t.string :email
      t.string :password
      t.string :university
      t.string :username
      t.string :image, default: ""
      t.boolean :is_active, default: true
      t.boolean :is_disabled, default: false
      t.timestamps
    end
  end
end
