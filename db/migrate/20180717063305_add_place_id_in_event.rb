class AddPlaceIdInEvent < ActiveRecord::Migration[5.2]
  def up
    add_column :events, :place_id, :bigint
    add_index :events, :place_id
  end
  def down
    remove_column :events, :place_id, :bigint
    remove_index :events, :place_id
  end
end
