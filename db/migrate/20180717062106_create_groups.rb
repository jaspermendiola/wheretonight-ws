class CreateGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :groups do |t|
      t.string :name
      t.belongs_to :user
      t.text :description
      t.boolean :for_students_only, default: false
      t.boolean :is_private, default: false
      t.string :item_photo, default: ""
      t.string :page_photo, default: ""
      t.integer :gender_inclusion
      t.boolean :is_active, default: true
      t.boolean :is_disabled, default: false
      t.timestamps
    end
  end
end
