class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.belongs_to :user
      t.boolean :is_read, default: false
      t.integer :type, default: 0
      t.string :image, default: ""
      t.timestamps
    end
  end
end
