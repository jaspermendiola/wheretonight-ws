class CreateEventUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :event_users do |t|
      t.belongs_to :user
      t.belongs_to :event
      t.integer :response, default: 0 #0 - UNDECIDED, 1 - MAYBE, 2 - DECLINE, 3 - ATTEND
      t.boolean :is_invited, default: false
      t.boolean :is_active, default: true
      t.boolean :is_disabled, default: false
      t.timestamps
    end
  end
end