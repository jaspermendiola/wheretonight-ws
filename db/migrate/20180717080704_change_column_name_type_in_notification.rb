class ChangeColumnNameTypeInNotification < ActiveRecord::Migration[5.2]
  def up
    rename_column :notifications, :type, :notification_type
  end
end