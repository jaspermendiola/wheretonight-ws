class CreatePlaces < ActiveRecord::Migration[5.2]
  def change
    create_table :places do |t|
      t.string :name
      t.string :image
      t.text :description
      t.string :address
      t.float :latitude
      t.float :longitude
      t.boolean :is_active, default: true
      t.boolean :is_disabled, default: false
      t.timestamps
    end
  end
end
