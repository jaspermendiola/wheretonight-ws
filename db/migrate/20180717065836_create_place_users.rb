class CreatePlaceUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :place_users do |t|
      t.belongs_to :user
      t.belongs_to :place
      t.boolean :is_liked, default: false
      t.boolean :is_checked_in, default: false
      t.boolean :is_active, default: true
      t.boolean :is_disabled, default: false
      t.timestamps
    end
  end
end