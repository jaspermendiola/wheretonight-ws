class CreatePostUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :post_users do |t|
      t.belongs_to :user
      t.float :latitude
      t.float :longitude
      t.string :content
      t.boolean :is_video, default: true
      t.boolean :is_active, default: true
      t.boolean :is_disabled, default: false
      t.timestamps
    end
  end
end