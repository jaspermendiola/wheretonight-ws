class AddImageUrlInEventTable < ActiveRecord::Migration[5.2]
  def up
    add_column :events, :image, :string
  end

  def down
    remove_column :events, :image, :string
  end
end