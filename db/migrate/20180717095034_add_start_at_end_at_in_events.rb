class AddStartAtEndAtInEvents < ActiveRecord::Migration[5.2]
  def up
    add_column :events, :start_at, :timestamp
    add_column :events, :end_at, :timestamp
  end
  def down
    remove_column :events, :start_at, :timestamp
    remove_column :events, :start_at, :timestamp
  end
end
