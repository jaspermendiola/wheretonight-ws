class RemoveLatitudeAndLongitudeThenAddPlaceIdInPostUser < ActiveRecord::Migration[5.2]
  def up
    remove_column :post_users, :latitude
    remove_column :post_users, :longitude
    add_column :post_users, :place_id, :bigint
    add_index :post_users, :place_id
  end
end