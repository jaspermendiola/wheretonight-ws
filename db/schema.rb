# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_07_18_004749) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "event_users", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "event_id"
    t.integer "response", default: 0
    t.boolean "is_invited", default: false
    t.boolean "is_active", default: true
    t.boolean "is_disabled", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_event_users_on_event_id"
    t.index ["user_id"], name: "index_event_users_on_user_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "title"
    t.text "about"
    t.bigint "user_id"
    t.boolean "is_private", default: false
    t.boolean "is_active", default: true
    t.boolean "is_disabled", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image"
    t.bigint "place_id"
    t.datetime "start_at"
    t.datetime "end_at"
    t.index ["place_id"], name: "index_events_on_place_id"
    t.index ["user_id"], name: "index_events_on_user_id"
  end

  create_table "follow_users", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "follow_id"
    t.boolean "is_active", default: true
    t.boolean "is_disabled", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_follow_users_on_user_id"
  end

  create_table "group_users", force: :cascade do |t|
    t.bigint "group_id"
    t.bigint "user_id"
    t.integer "status", default: 0
    t.boolean "is_active", default: true
    t.boolean "is_disabled", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_group_users_on_group_id"
    t.index ["user_id"], name: "index_group_users_on_user_id"
  end

  create_table "groups", force: :cascade do |t|
    t.string "name"
    t.bigint "user_id"
    t.text "description"
    t.boolean "for_students_only", default: false
    t.boolean "is_private", default: false
    t.string "item_photo", default: ""
    t.string "page_photo", default: ""
    t.integer "gender_inclusion"
    t.boolean "is_active", default: true
    t.boolean "is_disabled", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_groups_on_user_id"
  end

  create_table "menus", force: :cascade do |t|
    t.string "name"
    t.float "price"
    t.time "start_at"
    t.time "end_at"
    t.boolean "is_active", default: true
    t.boolean "is_disabled", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notifications", force: :cascade do |t|
    t.bigint "user_id"
    t.boolean "is_read", default: false
    t.integer "notification_type", default: 0
    t.string "image", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "place_users", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "place_id"
    t.boolean "is_liked", default: false
    t.boolean "is_checked_in", default: false
    t.boolean "is_active", default: true
    t.boolean "is_disabled", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["place_id"], name: "index_place_users_on_place_id"
    t.index ["user_id"], name: "index_place_users_on_user_id"
  end

  create_table "places", force: :cascade do |t|
    t.string "name"
    t.string "image"
    t.text "description"
    t.string "address"
    t.float "latitude"
    t.float "longitude"
    t.boolean "is_active", default: true
    t.boolean "is_disabled", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "post_users", force: :cascade do |t|
    t.bigint "user_id"
    t.string "content"
    t.boolean "is_video", default: true
    t.boolean "is_active", default: true
    t.boolean "is_disabled", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "place_id"
    t.index ["place_id"], name: "index_post_users_on_place_id"
    t.index ["user_id"], name: "index_post_users_on_user_id"
  end

  create_table "specials", force: :cascade do |t|
    t.bigint "place_id"
    t.bigint "menu_id"
    t.text "description"
    t.boolean "is_active", default: true
    t.boolean "is_disabled", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["menu_id"], name: "index_specials_on_menu_id"
    t.index ["place_id"], name: "index_specials_on_place_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "password"
    t.string "university"
    t.string "username"
    t.string "image", default: ""
    t.boolean "is_active", default: true
    t.boolean "is_disabled", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
