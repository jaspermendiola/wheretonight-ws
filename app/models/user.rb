class User < ApplicationRecord
    has_many :events
    has_many :follow_users
    has_many :group_users
    has_many :event_users
    has_many :place_users
    has_many :post_users
    has_many :notifications
end