class Event < ApplicationRecord
    belongs_to :user
    has_one :place
    has_many :event_users
end